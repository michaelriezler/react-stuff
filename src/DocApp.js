import React from 'react'
import { Switch, Route } from 'react-router-dom'

import './App.css'
import Appointments from './View/Appointments'
import NavBar, { NavItem } from './Components/NavBar'
import AppHeader, { DefaultHeader } from './Components/AppHeader'
import ActionHeader from './Components/AppHeader/ActionHeader'
import PatientDetail from './Components/PatientDetail'
import AppShell, { AppContent } from './Components/AppShell'


import Mail from './Components/Icon/Mail'
import Calender from './Components/Icon/Date'
import Folder from './Components/Icon/Folder'
import Euro from './Components/Icon/Euro'
import Person from './Components/Icon/Person'


let DocApp = () => (
  <AppShell>

    <AppHeader>
      <Switch>
        <Route
          onUpdate={() => window.scrollTo(0, 0)}
          path="/app/appointments/:id"
          component={ActionHeader} />
        <Route component={DefaultHeader} />
      </Switch>
    </AppHeader>
    
    <AppContent>
        <Route exact path="/app/:home(appointments|home)?" component={Appointments} />
        <Route path="/app/appointments/:id" component={PatientDetail} />
    </AppContent>
    
    <NavBar>
      <NavItem to="mail" icon={Mail} />
      <NavItem to="appointments" icon={Calender} />
      <NavItem to="patient" icon={Folder} />
      <NavItem to="billing" icon={Euro} />
      <NavItem to="colleges" icon={Person} />
    </NavBar>
    
  </AppShell>
)


export default DocApp
