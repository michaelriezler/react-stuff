import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import { Welcome } from '@storybook/react/demo';

import Card from '../Components/Card'
import { Demo as PatientList } from '../Components/Patient/List'
import Avatar, { Size as AvatarSize } from '../Components/Avatar'
import Login from '../Components/Login'


storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);


storiesOf('Cards/List', module)
    .addDecorator(story => (
        <div style={{ width : '300px'}}>
            {story()}
        </div>
    ))
	.add('Basic', () => 
        <PatientList />
	)
    .add('Active', () => 
        <PatientList active/>
    )
    .add('Long Name', () => 
        <PatientList active firstname="Lalawethika" lastname="Muraro"/>
    )

storiesOf('Cards/Overview', module)
	.add('Small', () => 
        <div style={{ width : '300px' }}>
            <Card>
            </Card>
        </div>
    )
    .add('Medium', () => 
        <div style={{ width : '600px' }}>
            <Card>
                
            </Card>
        </div>
    )
    .add('Large', () => 
		<div style={{ width : '900px' }}>
			<Card>
                
            </Card>
		</div>
	)


let avatar = size => (
    <Avatar
        img="http://via.placeholder.com/350x150"
        size={size}
        firstname="John"
        lastname="Doe"
    />
)

storiesOf('Avatar', module)
    .add('small', () => avatar(AvatarSize.Small))
    .add('medium', () => avatar(AvatarSize.Medium))
    .add('large', () => avatar(AvatarSize.Large))
    .add('default', () => avatar())


storiesOf('Login', module)
    .addDecorator(story => (
        <div style={{ height : '800px' }}>
            { story() }
        </div>
    ))
    .add('Default', () => <Login />)