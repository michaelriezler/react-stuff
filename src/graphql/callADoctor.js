import gql from 'graphql-tag'


export let CallADoctorNext = gql`
  mutation CallADoctorNext ($index : Int!) {
    callADoctorNext (index : $index) @client
  }
`

export let CallADoctorPrev = gql`
  mutation CallADoctorPrev ($index : Int!) {
    callADoctorPrev (index : $index) @client
  }
`

export let GetCurrentPage = gql`
  query CallADoctorCurrentPage {
    CallADoctor @client {
      __typename
      currentPage
      symptoms
    }
  }
`

export let SetLocation = gql`
  mutation CallADoctorSetAddress (
    $address : String!, 
    $coord : Coordinates!
  ) {
    setAddress (address : $address) @client
    setCoordinates(coord : $coord) @client
  }
`

export let GetLocation = gql`
  query CallADoctorGetAddress {
    CallADoctor @client {
      location {
        address
        coordinates {
          lat
          lng
        }
      }
    }
  }
`

export let SetCoordinates = gql`
  mutation CallADoctorSetCoordinates ($coord : Coordinates!) {
    setCoordinates (coord : $coord) @client
  }
`


export let GetDate = gql`
  query CallADoctorGetDate {
    CallADoctor @client {
      appointment {
        date
        time
      }
    }
  }
`

export let SetDate = gql`
  mutation CallADoctorSetDate ($date : Int!) {
    setDate(date : $date) @client
  }
`

export let SetTime = gql`
  mutation CallADoctorSetTime ($time : String!) {
    setTime (time : $time) @client
  }
`


export let GetFreeAppointments = gql`
  query CallADoctorFreeAppointments ($id : Int!, $date : Float!) {
    appointments (id : $id, date : $date, role : DOC, filter : FREE) {
      dateTime
    }
  }
`


export let GetSummary = gql`
  query CallADoctorSummary {
    CallADoctor @client {

      location {
        address
        coordinates {
          lat
          lng
        }
      }

      appointment {
        date
        time
      }
      
      symptoms

    }
  }
`


export let GetSymptoms = gql`
  query CallADoctorGetSymptoms ($bodyPart : BodyPart) {
    symptoms (bodyPart : $bodyPart) {
      id
      name
    }
  }
`

export let AddSymptom = gql`
  mutation CallADoctorAddSymptom (
    $symptomId : Int, 
    $name : String,
    $bodyPart : BodyPart
  ) {
    addSymptom (id : $symptomId, name : $name, bodyPart : $bodyPart) @client
  }
`

export let GetUserSymptoms = gql`
  query CallADoctorUserSymptoms {
    CallADoctor @client {
      symptoms
    }
  }
`


export let ScheduleAppointment = gql`
  mutation CallADoctorScheduleAppointment (
    $lng         : Float,
    $lat         : Float,
    $symptoms    : [Int],
    $patient     : Int,
    $doctor      : Int,
    $appointment : String,
    $duration    : Int  
  ) {
    scheduleAppointment (
      lat         : $lat
      lng         : $lng
      appointment : $appointment
      symptoms    : $symptoms
      patient     : $patient
      doctor      : $doctor
      duration    : $duration
    ) 
  }
`