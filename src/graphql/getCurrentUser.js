import gql from 'graphql-tag'


export let getCurrentUser = gql`
  query getCurrentUser {
    currentUser @client {
      role
      firstName
      lastName
    }
  }
`

export let getUserAddress = gql`
  query getUserAddress {
    currentUser @client {
      address {
        street
        city
        number
        postal
      }
    }
  }
`