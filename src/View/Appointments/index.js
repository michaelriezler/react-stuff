import React from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import PatientCard from '../../Components/Patient/List'
import './Appointments.css'


let Patient = ({
    id,
    patient,
    dateTime,
}) => (
    <div className="Appointments--Card">
        <PatientCard
            patient={patient}
            active={false}
            departAt={dateTime}
            appointment={Date.now()}
            id={patient.id}
        />
    </div>
)

let Appointments = ({ data }) => 
    data.loading
        ? <div>...</div> 
        : ( 
            <div className="ContentWrapper">
                <div>
                    { 
                        data.appointments
                            .map(i => <Patient key={i.id} {...i} />)
                    }
                </div>
            </div>
        )



let AppointmentList = gql`
    query Appointments {
        appointments (id : 1, role : DOC, filter : ALL) {
            id
            patient {
                id
                firstName
                lastName
                profileImage
            }
            dateTime
        }
    }
`


export default graphql(AppointmentList)(Appointments)