import React from 'react'
import './Login.css'
import Card from '../../Components/Card'
import Input from '../../Components/Input'
import { withRouter } from 'react-router-dom'


let handleSubmit = (cb = () => {}) => event => {
    event.preventDefault()
    cb()
}


let View = ({
    onSignIn
}) => (
    <div className="Login">
        <div className="Login-ContentWrapper">
            <Card>
                <form className="Login-Form" onSubmit={ handleSubmit(onSignIn) }>
                    <Input type="text" label="Username" />
                    <Input type="password" label="Password" />
                    <button type="submit">Login</button>
                </form>
            </Card>
        </div>
    </div>
)


export default withRouter(({ history }) => <View onSignIn={ () => history.push("/") } />)