import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { graphql, compose } from 'react-apollo'

import NavBar, { NavItem } from './Components/NavBar'
import AppHeader, { DefaultHeader } from './Components/AppHeader'
import CADHeader from './Components/AppHeader/CADHeader'
import AppShell, { AppContent } from './Components/AppShell'
import CallADoctor from './Components/CallADoctor'

import Doctor from './Components/Icon/Doctor'
import DateIcon from './Components/Icon/Date'
import Briefcase from './Components/Icon/Briefcase'
import Person from './Components/Icon/Person'

import { CallADoctorNext, 
         CallADoctorPrev, 
         GetCurrentPage,
         ScheduleAppointment,
         GetSummary,
       } from './graphql/callADoctor'


let Icons = () =>
  [ <NavItem key={1} to="call_a_doctor" icon={Doctor} />
  , <NavItem key={2} to="appointments" icon={DateIcon} />
  , <NavItem key={3} to="dontknow" icon={Briefcase} />
  , <NavItem key={4} to="user" icon={Person} />
  ]


let NoOp = label => () => { console.log(`${label} clicked`) }


let Buttons = ({ 
  onPrevious = NoOp('onPrevious'),
  onNext = NoOp('onNext'),
  data,
  summary,
  scheduleAppointment,
}) => {

  if (data.loading || data.error) return null

  let { CallADoctor : { currentPage } } = data

  let next_callback = page => () => {
    if (page < 3) {
      onNext({ variables : { index : currentPage + 1 } })
    } else {
      console.log(summary)
      scheduleAppointment({
        variables : { 
          patient : 1, 
          doctor : 2,
          lat : summary.location.lat,
          lng : summary.location.lng,
          symptoms : summary.symptoms.map(x => x.id),
          appointment : '',
          duration : 1234,
        }
      })
    }
  }

  return (
    [ <button
        className="Button"
        disabled={currentPage <= 0}
        key="prev"
        onClick={() => onPrevious({
          variables : { index : currentPage - 1 }
        })}
      >Previous</button>
    
    , <button
        className="Button"        
        disabled={currentPage >= 4}
        key="next"
        onClick={next_callback(currentPage)}
      >{ currentPage === 3 ? 'Send' : 'Next' }</button>
    ]
  )
}


let ButtonContainer = compose(
  graphql(CallADoctorPrev, { name : 'onPrevious' }),
  graphql(CallADoctorNext, { name : 'onNext' }),
  graphql(GetCurrentPage),
  graphql(ScheduleAppointment, { name : 'scheduleAppointment'}),
  graphql(GetSummary, {
    props ({ data }) {
      if (data.loading || data.error) 
        return { 
          summary : { 
            location : {},
            appointment : {},
            symptoms : []
          }
        }

      return { summary : { ...data.CallADoctor} }
    }
  })
)(Buttons)


export default () => (
  <AppShell>
    <AppHeader>
      <Switch>
        <Route 
          path="/app/call_a_doctor/get_location"
          component={DefaultHeader} 
        ></Route>
        
        <Route
          path="/app/call_a_doctor/"
          component={CADHeader}
        ></Route>

        <Route component={DefaultHeader} />
      </Switch>
    </AppHeader>

    <AppContent>
      <Switch>
        <Route path="/app/call_a_doctor/" component={CallADoctor} />
      </Switch>
    </AppContent>

    <NavBar>
      <Switch>
        <Route 
          path="/app/call_a_doctor/" 
          component={ButtonContainer}
        ></Route>
        <Route component={Icons} />
      </Switch>
    </NavBar>
  </AppShell>
)