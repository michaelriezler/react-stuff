import React from 'react'
import ComponentLoader from './Components/ComponentLoader'


let App = () => (
  <ComponentLoader
    doc={() => import('./DocApp.js')}
    client={() => import('./PatientApp.js')}
  ></ComponentLoader>
)


export default App