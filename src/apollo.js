import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { withClientState } from 'apollo-link-state'
import { merge } from 'lodash-es'
import currentUser from './apollo/currentUser'
import CallADoctor, { initalState as CADInitalState} from './apollo/CallADoctor'


let cache = new InMemoryCache()


let defaults = {
  CallADoctor : CADInitalState
}


let resolvers = merge(
  currentUser,
  CallADoctor,
)


let stateLink = withClientState({
  cache,
  resolvers,
  defaults,
})


let link = ApolloLink.from([
  stateLink, 
  new HttpLink('/graphql')
])


export default new ApolloClient({
  link,
  cache,
})