import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'

import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import Login from './View/Login'
import ApolloClient from './apollo'


let Main = () => (
    <ApolloProvider client={ApolloClient}>
        <Router>
            <Switch>
                <Route path="/login/" component={Login} />
                <Route path="/app/" component={App} />
                <Route render={() => <Redirect to="/app/home/" />} />
            </Switch>
        </Router>
    </ApolloProvider>
)


ReactDOM.render(<Main />, document.getElementById('root'))
registerServiceWorker()
