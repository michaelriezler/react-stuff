import React from 'react'
import './Avatar.css'    


export let Size = 
    { Small : 'SMALL'
    , Medium : 'MEDIUM'
    , Large : 'LARGE'
    }


let get_size = size => {
    switch (size) {
        case 'SMALL' : return 'small'
        case 'MEDIUM' :
        default : return 'medium'
        case 'LARGE' : return 'large'
    }
}


export default props => (
    <div className={`Avatar Avatar--size_${get_size(props.size)}`}>
    	<div className="Avatar--circle">
    		<img 
                src={ props.img } 
                alt={`Patient ${props.firstname}, ${props.lastname}`} 
            />
    	</div>
    </div>
)