import { Component } from 'react'


export class ScrollToTopOnMount extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  render() {
    return this.props.children
  }
}