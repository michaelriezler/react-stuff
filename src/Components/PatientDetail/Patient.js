import React from 'react'
import Card from '../Card'


let make_alt = ({ firstName, lastName }) => 
  `${firstName} ${lastName}`


let Patient = ({
  patient,
  nextAppointment,
}) => (
  <div>
    <Card className="PatientDetail-Patient PatientDetail-Card">
      <div className="PatientDetail-ImageWrapper">
        <img src={patient.profileImage} alt={make_alt(patient)}/>
      </div>
      <div>
        <h2 className="PatientDetail-Name">{patient.firstName} {patient.lastName}</h2>
        <p>{patient.birthday}</p>
      </div>

      <p>{patient.insurance}</p>

      <div className="PatientDetail-Address">
        <p>{patient.address.street}</p>
        <p>{patient.address.postal} {patient.address.city}</p>
      </div>
    </Card>

    <div>
      <h3>Next Appointment</h3>
      <Card className="PatientDetail-Card">
        <h4>{nextAppointment.dateTime}</h4>
        <p>Abfahrt: </p>
        <p>Termin: </p>
      </Card>
    </div>
  </div>
)


export default Patient