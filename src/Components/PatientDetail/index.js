import React from 'react'
import './PatientDetail.css'
import { ScrollToTopOnMount } from '../Router'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import Tab, { Item as TabItem, TabContainer, TabContent, Utils } from '../Tab'


let PatientDetail = ({
  data : { loading, patient, nextAppointment },
}) => 
  loading ? null : (
  <ScrollToTopOnMount>
    <TabContainer>

      <Tab sticky top={0} selected="patient" >
        <TabItem select="patient">Patient</TabItem>
        <TabItem select="documents">Documents</TabItem>
      </Tab>

      <TabContent 
        className="PatientDetail"
        select="patient"
        component={() =>
          import('./Patient.js').then(Utils.mapProps({
              patient,
              nextAppointment,
            })
          )
        }
      ></TabContent>

      <TabContent 
        select="documents"
        component={() => import('./Documents.js').then(Utils.createElement)}
      ></TabContent>

    </TabContainer>
  </ScrollToTopOnMount>
)


let PATIENT_QUERY = gql`
  query GetPatient ($patientId : Int!) {
    patient (id : $patientId) {
      id
      firstName
      lastName
      email
      phone
      profileImage
      address {
        street
        postal
        city
      }
      birthday
      insurance
    }

    nextAppointment(patientId: $patientId) {
      dateTime
    }
  }
`

export default graphql(
  PATIENT_QUERY, 
  { options : ({ match }) => ({
      variables : { patientId : match.params.id }
    }),
  },
)(PatientDetail)