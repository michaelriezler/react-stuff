import React from 'react'
import Tab, { Item as TabItem, TabContainer } from '../Tab'


export default () => (
  <TabContainer>
    <Tab sticky top={64} className="SubTab" selected="Verlauf">
      <TabItem select="Verlauf">Verlauf</TabItem>
      <TabItem select="Allergien">Allergien</TabItem>
      <TabItem select="Medikamentenplan">Medikamentenplan</TabItem>
      <TabItem select="Impfungen">Impfungen</TabItem>
    </Tab>
  </TabContainer>
)