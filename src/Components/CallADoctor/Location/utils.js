import React from 'react'
import GoogleMapReact from 'google-map-react'
import { path } from 'ramda'


let KEY = 'AIzaSyAmQys3YCcpS3QUgVJVCGey58UdGb_9R4Y'

export let getCurrentPosition = () => 
  new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(function(position) {
      resolve(position.coords)
    })
  })


export let getAddress = ({ longitude, latitude }) => 
  fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${KEY}`)
  .then(res => res.json())


export let getCoordinates = address => {
  address = encodeURIComponent(address)
  return fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${KEY}`)
    .then(res => res.json())
    .then(path(['results', '0', 'geometry', 'location']))
}


export let GoogleMap = ({ marker, center }) => 
  <GoogleMapReact
    bootstrapURLKeys={{
      key : 'AIzaSyCWowRg6yBUkuraCDa71W2timTyv_YSjro'
    }}
    center={center}
    defaultZoom={15}
  >{marker.map(make_marker)}</GoogleMapReact>




let make_marker = ({
  location,
}, key) => {
  return <Marker
    key={key}
    {...location}
  ></Marker>
}


  let Marker = () => <div className="CADLocation-GetAddress--Marker"></div>