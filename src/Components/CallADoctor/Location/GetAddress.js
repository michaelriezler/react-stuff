import React, { Component } from 'react'
import { noop } from 'lodash-es'
import { GoogleMap } from './utils'


class LocationGetAddress extends Component {
  constructor () {
    super()

    this.state = {
      coordinates : { lat: 10, lng: 10}
    }
  }

  componentDidMount () {
    let [best_result] = this.props.addresses
    this.setState({
      coordinates : best_result.location
    })
  }

  render () {
    let { addresses = [], onSetAddress = noop } = this.props
    let { coordinates } = this.state
    return (
      <div className="CADLocation-GetAddress">
        <div className="CADLocation-GetAddress--Map">
          <GoogleMap
            marker={addresses}
            center={coordinates}
          ></GoogleMap>
        </div>
        <ul
          className="CADLocation-GetAddress--List"
        >{ 
          addresses.map((x, i) => 
            <li
              key={i}
              onClick={() => onSetAddress(x.formatted_address, x.location)}
            >{x.formatted_address}</li>
          )
        }</ul>
      </div>
    )
  }
}


export default LocationGetAddress