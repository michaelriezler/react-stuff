import React from 'react'
import { noop } from 'lodash-es'
import { getCurrentPosition, getAddress, GoogleMap, getCoordinates } from './utils'



export default ({
  address = '',
  onSetAddress =  noop,
  userAddress = {},
  onGetAddress = noop,
  coordinates
}) => (
  <div className="CADLocation">
    <div className="CADLocation-InputWrapper">
      <h3>Pick the location of the appointment</h3>
      <div className="CADLocation-Action">
        <button
          className="Button"
          onClick={ () => {
              let address = address_to_string(userAddress) 
              getCoordinates(address)
                .then(coordinates => onSetAddress(address, coordinates))
            }
          }
        >Home</button>
        <button
          className="Button"
          disabled={false === "geolocation" in navigator}
          onClick={_ => {
            getCurrentPosition()
              .then(getAddress)
              .then(x => x.results.map(address => ({
                  formatted_address : address.formatted_address,
                  location : address.geometry.location,
                }))
              )
              .then(onGetAddress)
          }}
        >Current Location</button>
      </div>

      <div className="CADLocation-Input">
        <input 
          type="text"
          value={address}
          onChange={event => {
            onSetAddress(event.target.value)
          }}
        />
      </div>
    </div>
    <div className="CADLocation-Map">
      <GoogleMap
        marker={[{ location : coordinates}]}
        center={coordinates}
      ></GoogleMap>
    </div>
  </div>
)


let address_to_string = ({
  street = '',
  city = '',
  number = '',
  postal = '',
}) => `${street} ${number}, ${postal} ${city}`