import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import { path } from 'ramda'

import { getUserAddress } from '../../graphql/getCurrentUser'
import { 
  SetLocation,
  GetLocation,
  SetCoordinates,
} from '../../graphql/callADoctor'
import { getCurrentPosition } from './Location/utils'

import Overview from './Location/Overview'
import PickAddress from './Location/GetAddress'

import './Location/Location.css'




let View = {
  Overview : 'OVERVIEW',
  GetAddress : 'GET_ADDRESS',
}


class CADLocation extends Component {
  
  constructor () {
    super()

    this.state = {
      view : View.Overview,
      addresses : [],
    }
  }

  componentDidMount () {
    getCurrentPosition().then(coord => this.props.setCoordinates({
      variables : {
        coord : {
          lat : coord.latitude,
          lng : coord.longitude,
        },
      },
    }))
  }

  render() {
    let { 
      loading, 
      error, 
      address,
      setLocation,
      userAddress,
      coordinates,
    } = this.props

    let { view, addresses } = this.state

    if (loading || error) return null

    return View.Overview === view
      ? <Overview
          address={address}
          onSetAddress={(address, coord) => {
            setLocation({ variables : { address, coord } })
          }}
          userAddress={userAddress}
          onGetAddress={(addresses, redirect) =>
            this.setState({ 
              addresses,
              view : redirect ? View.GetAddress : this.state.view,
            })
          }
          coordinates={coordinates}
        ></Overview>

      : <PickAddress
          addresses={addresses}
          onSetAddress={(address, coord) => {
            setLocation({ variables : { address, coord }})
            this.setState({
              addresses : [],
              view : View.Overview,
            })
          }}
        ></PickAddress>
  }
}


let get = (prop, obj) =>
  path(['CallADoctor', 'location', prop], obj)


export default compose(
  graphql(getUserAddress, {
    props: ({ data }) => ({
      loading : data.loading,
      error : data.error,
      userAddress : path(['currentUser', 'address'], data),
    })
  }),

  graphql(GetLocation, { 
    props : ({ data, ownProps }) =>
      (data.loading || data.error) 
        ? ({ address : '', coordinates : { lat : 0, lng : 0 } })
        : ({ 
            address : get('address', data),
            coordinates : get('coordinates', data)
          })
  }),

  graphql(SetCoordinates, { name : 'setCoordinates'}),
  graphql(SetLocation, { name : 'setLocation'}),
)(CADLocation)