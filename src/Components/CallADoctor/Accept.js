import React from 'react'
import { graphql, compose } from 'react-apollo'
import { GetSummary } from '../../graphql/callADoctor'


let Overview = ({
  location,
  appointment,
  symptoms
}) => (
  <div>
    <h2>Summary</h2>  
    <section>
      <h3>Symptoms</h3>
      <ul>
        { symptoms.map(x => <li key={x.id}>{x.name}</li>)}
      </ul>
    </section>

    <section>
      <h3>Appointment</h3>
      <p>Address:{ location.address }</p>
      <p>Date:{ appointment.date }</p>
      <p>Time:{ appointment.time }</p>
    </section>
  </div>
)


export default compose(
  graphql(GetSummary, {
    props ({ data }) {
      if (data.loading || data.error) 
        return {
          location : {},
          appointment : {},
          symptoms : []
        }

      return { ...data.CallADoctor }
    }
  })
)(Overview)