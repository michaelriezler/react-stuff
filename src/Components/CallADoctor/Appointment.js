import React from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import moment from 'moment'
import { GetDate, 
         SetDate, 
         SetTime,
         GetFreeAppointments } from '../../graphql/callADoctor'
import { compose, graphql } from 'react-apollo'
import { path } from 'ramda'
import Card from '../Card'


let make_card = onClick => (start, index) =>
  <Card
    style={ { marginBottom : 16 } }
    key={index}
    onClick={ () => onClick(start.format('HH:mm'))}
  >
    { start.format('hA, ddd') }
  </Card>


let make_time = time => ({
  variables : { time }
})


let CADAppointment = ({
  date,
  appointments,
  setDate,
  setTime,
  time,
}) => (
  <div className="CAD-Appointment">
    <div className="CAD-Appointment--Datepicker">
      <DatePicker
        selected={date}
        onChange={date => 
          setDate({
            variables : {
              date : date.valueOf()
            }
          })
        }
      ></DatePicker>
      <p>{ time }</p>
    </div>
    <div>
      { appointments.map(make_card(time => setTime(make_time(time)))) }
    </div>
  </div>
)


export default compose(
  graphql(GetDate, {
    props ({ data }) {
      let date = path(['CallADoctor', 'appointment', 'date'], data)
      
      if (data.loading || data.error) return {
        date : moment(),
        time : ''
      }
      
      return { 
        date : moment(date),
        time : path(['CallADoctor', 'appointment', 'time'], data)
      }
    }
  }),

  graphql(SetDate, { name : 'setDate' }),
  graphql(SetTime, { name : 'setTime' }),

  graphql(GetFreeAppointments, {
    options: ({ date }) => ({
      variables : {
          id : 1,
          date : moment(date).valueOf(),
        }
    }),

    props ({ data }) {
      if (data.loading || data.error) return { appointments : [] }
      return {
        appointments : data.appointments.map(x => moment(x))
      }
    }
  })
)(CADAppointment)