import React, { Component } from 'react'
import { graphql, compose } from 'react-apollo'
import './Symptoms/Symptoms.css'

import { GetSymptoms, 
         AddSymptom, 
         GetUserSymptoms, 
       } from '../../graphql/callADoctor'

import Dude from '../Icon/Dude'
import { noop } from 'lodash-es'


let ListItem = ({
  id,
  name,
  onClick,
  bodyPart,
}) => (
  <li onClick={() => onClick({
    variables : { symptomId : id, name, bodyPart }
  })}>{ name }</li>
)


let Drawer = ({
  symptoms,
  bodyPart,
  onAddSymptom = noop,
}) => (
  <div className={`Symptoms--Drawer ${null === bodyPart ? 'hidden' : ''}`}>
    <h3>{ bodyPart }</h3>
    <input type="text" placeholder="search or create symptom"/>
    <ul>
      { symptoms.map(x => 
          <ListItem 
            {...x}
            key={x.id} 
            onClick={onAddSymptom}
            bodyPart={bodyPart}
          ></ListItem>
        ) 
      }
    </ul>
  </div>
)


Drawer = compose(
  graphql(GetSymptoms, {

    options : ({ bodyPart }) => ({
      variables : { bodyPart  }
    }),
    

    props : ({ data : { loading, error, symptoms } }) => {
      if (error || loading) return { symptoms : [] }
      return { symptoms }
    }
  })
)(Drawer)


class Symptoms extends Component {

  constructor () {
    super()

    this.state = {
      drawerOpen : false,
      bodyPart : null,
    }
  }

  render () {
    return (
      <div className="Symptoms">
        <div className="Symptoms-Icon-Wrapper">
          <Dude
            onBodyPartClicked={({ detail }) => this.setState({ ...detail })}
            active={this.state.bodyPart}
          ></Dude>
        </div>
        <Drawer
          bodyPart={this.state.bodyPart}
          onAddSymptom={this.props.addSymptom}
        ></Drawer>

        <div className="Symptoms-Symptom-List">
          <ul>{
            this.props.symptoms.map(s => <li key={s.id}>{s.name}</li>)
          }</ul>
        </div>
      </div>
    )
  }
}

export default compose(
  graphql(AddSymptom, { name : 'addSymptom'}),

  graphql(GetUserSymptoms, {
    props ({ data }) {
      if (data.loading || data.error) return { symptoms : [] }
      return { symptoms : data.CallADoctor.symptoms }
    }
  }),

)(Symptoms)