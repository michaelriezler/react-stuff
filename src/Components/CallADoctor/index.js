import { Component, createElement } from 'react'
import { GetCurrentPage } from '../../graphql/callADoctor'
import { compose, graphql } from 'react-apollo'
import { path } from 'ramda'


let getCurrentPage = path(['CallADoctor', 'currentPage'])


class CallADoctor extends Component {
  constructor () {
    super()

    this.state = {
      Component : null
    }


    this.componentMap = new Map(
      [ [0, () => import('./Symptoms.js')]
      , [1, () => import('./Location.js')]
      , [2, () => import('./Appointment.js')]
      , [3, () => import('./Accept.js')]
      ]
    )
  }


  componentWillReceiveProps({ currentPage }) {
    let fn = this.componentMap.get(currentPage % 4)
    fn().then(({ default : Component }) => this.setState({ Component }))
  }


  render () {
    let { Component } = this.state
    return Component ? createElement(Component) : null
  }
}

export default compose(
  graphql(GetCurrentPage, { props : ({ data }) => ({
      currentPage : getCurrentPage(data)
    })
  })
)(CallADoctor)