import React, { cloneElement } from 'react'
import { noop } from 'lodash-es'


let withClick = (active, fn) => ({ component, label }) => {
  let style = { fill : label === active ? '#f0f' : null }
  
  let onClick = (event) => {
    event.stopPropagation()
    fn({ ...event, detail : { bodyPart : label } })
  }

  return cloneElement(component, {
    style, onClick, key : label
  })
}


export let BodyPart = {
  Head     : 'HEAD',
  ArmLeft  : 'LEFT_ARM',
  ArmRight : 'RIGHT_ARM',
  LegRight : 'RIGHT_FEET',
  LegLeft  : 'LEFT_FEET',
  Stomach  : 'TUMMY',
}


let bodyParts = 
  [ { component : <circle id="Head" cx="39.999" cy="9.991" r="10.009"/>
    , label : BodyPart.Head
    }
  
  , { component : <path id="ArmLeft" d="M29.063,26.066c0,0 -15.756,0 -24.042,0c-1.316,0 -2.578,0.523 -3.509,1.453c-0.931,0.931 -1.453,2.193 -1.453,3.509c0,0.001 0,0.001 0,0.002c0,1.317 0.523,2.58 1.456,3.511c0.932,0.931 2.196,1.453 3.513,1.451c6.619,-0.026 17.769,-0.06 20.12,-0.01c0,0 0,0 0,0c3.391,0.072 3.915,3.467 3.915,3.467l0,-13.383Z"/>
    , label : BodyPart.ArmLeft
    }
  
  , { component : <path id="ArmRight" d="M50.993,26.066c0,0 15.756,0 24.043,0c1.316,0 2.578,0.523 3.508,1.453c0.931,0.931 1.454,2.193 1.454,3.509c0,0.001 0,0.001 0,0.002c0,1.317 -0.524,2.58 -1.456,3.511c-0.932,0.931 -2.196,1.453 -3.514,1.451c-6.618,-0.026 -17.769,-0.06 -20.12,-0.01c0,0 0,0 0,0c-3.391,0.072 -3.915,3.467 -3.915,3.467l0,-13.383Z"/>
    , label : BodyPart.ArmRight
    }

  , { component : <path id="LegLeft" d="M40.112,56.691l-11.081,-0.025c-0.681,13.292 -1.716,23.974 -2.897,37.973c-0.232,2.753 1.801,5.176 4.552,5.426c0.001,0 0.001,0 0.001,0c1.457,0.132 2.907,-0.327 4.021,-1.275c1.115,-0.948 1.802,-2.304 1.906,-3.764c0.661,-10.487 2.073,-32.373 2.457,-34.499c0.082,-0.454 0.648,-0.653 0.968,-0.675c0.004,-0.001 0.073,-3.161 0.073,-3.161Z"/>
    , label : BodyPart.LegLeft
    }

  , { component : <path id="LegRight" d="M39.849,56.693l11.186,-0.022c0.527,13.861 1.633,24.07 2.83,37.947c0.239,2.762 -1.801,5.198 -4.562,5.448c-0.001,0 -0.002,0 -0.003,0c-1.459,0.133 -2.909,-0.328 -4.025,-1.277c-1.115,-0.949 -1.801,-2.308 -1.904,-3.769c-0.699,-10.474 -2.169,-32.302 -2.417,-34.446c-0.043,-0.377 -0.595,-0.7 -0.915,-0.722c-0.015,-0.001 -0.19,-3.159 -0.19,-3.159Z"/>
    , label : BodyPart.LegRight
    }

  , { component : <rect id="Stomach" x="28.998" y="26.045" width="22.055" height="30.754"/>
    , label : BodyPart.Stomach
    }
  ]


export default ({
  onBodyPartClicked = noop,
  active = null,
}) => (
  <svg 
    viewBox="0 0 80 100" 
    xmlns="http://www.w3.org/2000/svg"
    onClick={() => onBodyPartClicked({ detail : { bodyPart : null } })}
  >{ bodyParts.map(withClick(active, onBodyPartClicked)) } </svg>
)