import React from 'react'
import './List.css'
import Avatar, { Size as AvatarSize } from '../Avatar'
import Location from '../Icon/Location'
import Phone from '../Icon/Phone'
import Mail from '../Icon/Mail'
import IconButton from '../IconButton'
import { NavLink } from 'react-router-dom'


let PatientList = ({
    active,
    patient,
    departAt,
    appointment,
    id,
}) => (
    <div className={`PatientList ${ active ? 'PatientList--Active' : '' }`}>
        <NavLink 
            to={`/app/appointments/${id}`}
            className="PatientList-Link"
        >
            <div className="PatientList-Content">
                <Avatar
                    img={patient.profileImage}
                    firstname={patient.firstName}
                    lastname={patient.lastName}
                    size={AvatarSize.Medium}
                />
                <p>
                    {`${patient.firstName} ${patient.lastName}`}
                </p>
                <p className="PatientList-DepartAt">
                    Abfahrt: {departAt}
                </p>
                <p className="PatientList-Appointment">
                    Termin: {appointment}
                </p>
            </div>
        </NavLink>
        <div className="PatientList-Icons">
          <IconButton icon={Location} />
          <IconButton icon={Phone} />
          <IconButton icon={Mail} />
        </div>
    </div>
)


export default PatientList


let DemoPatient = (firstname = 'John', lastname = 'Doe') => (
    { img : 'http://via.placeholder.com/350x350'
    , firstname
    , lastname 
    }
)

export let Demo = ({ active, firstname, lastname }) => 
    <PatientList
        active={ active ? true : false }
        patient={ DemoPatient(firstname, lastname) }
        departAt="11:30"
        appointment="11:40"
    />