import React from 'react'
import { NavLink } from 'react-router-dom'
import './NavBar.css'


export let NavItem = ({
    icon : Icon,
    to,
}) => (
    <NavLink className="NavItem" to={`/app/${to}`}>
        <Icon />
    </NavLink>
)





export default props => (
    <nav className="NavBar">
        { props.children }
    </nav>
)