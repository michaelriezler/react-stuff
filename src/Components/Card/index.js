import React from 'react'
import './Card.css'


export default ({
  children,
  className,
  ...props,
}) => (
	<div className={`Card ${className}`} {...props}>
		{ children }
	</div>
)