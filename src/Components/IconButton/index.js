import React from 'react'
import './IconButton.css'


export default ({ 
  icon : Icon,
  className,
  ...props
}) => (
  <button className={`IconButton ${className}`} {...props}>
    <Icon />
  </button>
)