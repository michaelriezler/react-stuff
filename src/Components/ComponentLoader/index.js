import React, { Component, createElement } from 'react'
import { Redirect, withRouter } from 'react-router-dom'
import { graphql } from 'react-apollo'
import { getCurrentUser } from '../../graphql'


let componentWithRouter = ({ default : component }) => withRouter(component)


class ComponentLoader extends Component {
  constructor () {
    super()

    this.state = {
      Component : null
    }

    this.setComponent = this.setComponent.bind(this)
  }


  componentWillReceiveProps({ data }) {
    let { doc, client } = this.props
    
    if (undefined !== data.error)
        console.log(data.error)

    if (null === data.currentUser) return

    if ('DOC' === data.currentUser.role) this.setComponent(doc)
    if ('CLIENT' === data.currentUser.role) this.setComponent(client)
  }

  
  async setComponent (fn) {
    let Component = await fn().then(componentWithRouter)
    this.setState({ Component })
  }


  render () {
    let { loading, currentUser } = this.props.data
    let { Component } = this.state

    if (true === loading) return <div>...</div>
    if (null === currentUser) return <Redirect to="/login" />
    
    return Component ? createElement(Component) : null
  }
}


export default graphql(getCurrentUser)(ComponentLoader)