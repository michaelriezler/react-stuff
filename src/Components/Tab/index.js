import React, { Component, Children, cloneElement } from 'react'
import './Tab.css'


class Tab extends Component {
  constructor() {
    super()

    this.state = {
      selected : '',
      top: 0
    }

    this.handleScroll = this.handleScroll.bind(this)
    this.container = null
  }

  componentDidMount() {
    
    if (this.props.sticky) {

      window.addEventListener('scroll', this.handleScroll)
    }

    this.setState({ 
      selected : this.props.selected,
      top: this.container.getBoundingClientRect().y,
    })
  }

  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll (event) {
    let sticky = this.state.top - this.props.top - window.scrollY < 0
    this.setState({ sticky })
  }

  render () {
    let { children, onSelected, className, top } = this.props
    let { selected, sticky : isSticky } = this.state

    return (
      <div>
        <div 
          className={`Tab ${className || ''}`}
          ref={node => this.container = node }
          style={ isSticky ? { position : 'fixed', top } : {}}
        >
          { Children.map(children, child => {
              let { props } = child
              child = cloneElement(
                child,
                { onClick : () => {
                    this.setState({ selected : props.select })
                    if (onSelected) onSelected(props.select)
                  }
                , active: props.select === selected
                }
              )
              return child
            }) 
          }
        </div>
        <div className="Tab-Wrapper"></div>
      </div>
    )
  }
}

export default Tab


export class TabContainer extends Component {
  constructor() {
    super()

    this.state = {
      selected: '',
      Controls: null,
      Body: []
    }
  }

  componentDidMount () {
    let children = Children.toArray(this.props.children)
    let [Controls] = children.filter(item => item.type === Tab)
    let Body = children.filter(item => item.type === TabContent)

    Controls = cloneElement(Controls, {
      onSelected: selected => this.setState({ selected })
    })

    this.setState({ 
      selected : Controls.props.selected,
      Controls,
      Body,
    })
  }

  render () {
    let { Controls, Body, selected } = this.state
    let Item = Body.filter(item => item.props.select === selected)

    return (
      <div>
        { Controls }
        { Item }
      </div>
    )
  }
}


export class TabContent extends Component {
  constructor () {
    super()

    this.state = {
      component : null
    }
  }

  componentDidMount() {
    this.props.component().then(component => this.setState({ component }))
  }

  render() {
    let { children, component, ...props } = this.props
    return <div {...props}>{ this.state.component }</div>
  }
}


export let Item = ({
  children,
  className,
  active,
  ...props,
}) => (
  <button 
    className={`Tab-Item ${className || ''} ${active ? 'Tab-Item--active' : ''}`} {...props}
  >{children}</button>
)


let mapProps = props => ({ default : Comp }) =>
  React.createElement(Comp, props)


let createElement = ({ default : Comp }) =>
  React.createElement(Comp)


export let Utils = {
  mapProps,
  createElement,
}