import React from 'react'
import ArrowBack from '../Icon/ArrowBack'
import './CADHeader.css'


let CADHeader = ({
  history,
}) => (
  <div className="CADHeader">
    <button
      className="Button--Back"
      onClick={history.goBack}
    >
      <ArrowBack /> Call A Doctor
    </button>
  </div>
)


export default CADHeader