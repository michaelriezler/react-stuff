import React from 'react'
import Location from '../Icon/Location'
import Phone from '../Icon/Phone'
import Mail from '../Icon/Mail'
import IconButton from '../IconButton'


let ActionHeader = ({ 
  history,
  email,
  phone,
  location,
  client,
}) => (
  <div className="ActionHeader">
    <div>
      <button 
        className="Button-Close" 
        onClick={() => history.goBack()}
      >X</button>
    </div>
    <div className="ActionHeader-Buttons">
      <IconButton className="ActionHeader-Buttons-Button" icon={Location} />
      <a className="ActionHeader-Buttons-Button" href={`tel:${phone}`}>
        <IconButton icon={Phone} />
      </a>
      <a className="ActionHeader-Buttons-Button" href={`mailto:${email}`}>
        <IconButton icon={Mail} />
      </a>
    </div>
  </div>
)


export default ActionHeader