import React, { Component } from 'react'
import './AppHeader.css'
import { throttle } from 'lodash-es'


let handleScroll = self => throttle( 
    () => {
        let { scrollY } = self.state
        let { scrollY : windowY } = window
        let down = scrollY < windowY
        let scrollDirection = down ? 'DOWN' : 'UP'
        self.setState({ scrollY : windowY, scrollDirection })
    },
    100,
)

export default class AppHeader extends Component {
    constructor() {
        super()

        this.state = {
            scrollDirection: 'UP',
            scrollY: window.scrollY,
        }

    }

    componentDidMount() {
        window.addEventListener('scroll', handleScroll(this))
    }

    render() {
        let hide = 'DOWN' === this.state.scrollDirection

        return (
            <div className={`AppHeader ${hide ? 'AppHeader--hide' : ''}`}>
                { this.props.children }
            </div>
        )
    }
}


export let DefaultHeader = () => 
    <h2 className="AppHeader-Title">Dr. Who</h2>