import React from 'react'
import './AppShell.css'


export let AppContent = ({ children }) => (
  <div className="App-Content">
    { children }
  </div>
)

export default ({ children }) => (
  <div className="App">
    { children }
  </div>
)