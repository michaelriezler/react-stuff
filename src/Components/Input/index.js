import React from 'react'
import './Input.css'


export default ({ 
    type,
    label
}) => (
    <div className="Input">
        <input 
            className="Input-NativeInput"
            type={ type ? type : 'text'} 
            placeholder={ label ? label : null }
        />
    </div>
)