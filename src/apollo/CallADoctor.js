import gql from 'graphql-tag'

let Mutation = {
  addSymptom (_, { id, name, bodyPart }, { cache }) {
    let query = gql`
      query GetSymptoms {
        CallADoctor @client {
          symptoms
        }
      }
    `

    let previous = cache.readQuery({ query })
    
    let newSymptom = {
      id,
      name,
      affectedBodyPart : bodyPart,
      __typename: 'Symptom'
    }

    let data = {
      CallADoctor : {
      ...previous.CallADoctor,
      symptoms : previous.CallADoctor.symptoms.concat([newSymptom])
      }
    }

    cache.writeQuery({ query, data })
    return newSymptom
  },

  callADoctorNext (_, { index }, { cache }) {
    let CallADoctor = {
      currentPage : index,
      __typename : 'CallADoctor',
    }

    cache.writeData({ data : { CallADoctor }})
    return CallADoctor
  },

  callADoctorPrev (_, { index }, { cache }) {
    let CallADoctor = {
      currentPage : index,
      __typename : 'CallADoctor',
    }

    cache.writeData({ data : { CallADoctor }})
    return CallADoctor
  },

  setAddress (_, { address }, { cache }) {
    let CallADoctor = {
      location : { 
        address,
        __typename : 'Location',
      },
      __typename : 'CallADoctor',
    }

    cache.writeData({ data : { CallADoctor } })
    return CallADoctor
  },

  setCoordinates (_, { coord }, { cache }) {
    let CallADoctor = {
      location : {
        coordinates : { 
          ...coord,
          __typename : 'Coordinates',
        },
        __typename : 'Location',
      },
      __typename : 'CallADoctor',
    }

    cache.writeData({ data : { CallADoctor } })
    return CallADoctor
  },

  setDate (_, { date }, { cache }) {

    let CallADoctor = {
      appointment : {
        date,
        __typename : 'Appointment',
      },
      __typename : 'CallADoctor',
    }

    cache.writeData({ data : { CallADoctor } })
    return CallADoctor
  },

  setTime (_, { time }, { cache }) {

    let CallADoctor = {
      appointment : {
        time,
        __typename : 'Appointment',
      },
      __typename : 'CallADoctor',
    }

    cache.writeData({ data : { CallADoctor } })
    return CallADoctor
  },

}


let Query = {
  location () {
    return {
      address : '',
    }
  }
}


export let initalState = {
  currentPage : 0,
  location : {
    address : '',
    coordinates : {
      lat : 0,
      lng : 0,
    __typename : 'Coordinates',
    },
    __typename : 'Location',
  },
  appointment : {
    date : Date.now(),
    time : '',
    __typename : 'Appointment',
  },
  symptoms : [],
  __typename : 'CallADoctor'
};


export default { Mutation, Query }
