

let Query = {
  currentUser : async () => {
    return { 
      id : 1233,
      role : 'DOC',
      firstName : 'John',
      lastName : 'Doe',
      __typename : 'currentUser',
    }
  }
}

let currentUser = {
  address : () => {
    return ({
      street : 'Rotebühlplatz',
      city : 'Stuttgart',
      number : 41,
      postal : 70178,
      __typename : 'currentUserAddress',
    })
  },
}


export default { Query, currentUser }