import React from 'react'
import { configure, addDecorator } from '@storybook/react';

function loadStories() {
  require('../src/stories')
  require('../src/index.css')
}


let styles =
    { padding: '32px'
    }

addDecorator(story => (
  <div style={styles}>
    { story() }
  </div>
))

configure(loadStories, module);
