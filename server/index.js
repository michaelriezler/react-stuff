let express = require('express')
let bodyParser = require('body-parser')
let { graphqlExpress, 
      graphiqlExpress
} = require('apollo-server-express')
let schema = require('./data/schema')
let PORT = 3001
let app = express()


app.use(
    '/graphql', 
    bodyParser.json(),
    graphqlExpress({ schema: schema })
)


app.use(
    '/graphiql',
    graphiqlExpress({ endpointURL: '/graphql' })
)


app.listen(
    PORT, 
    () => console.log(`GraphQL server is running on port ${PORT}`)
)