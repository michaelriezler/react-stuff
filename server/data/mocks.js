let _ = require('faker')
let { MockList } = require('graphql-tools')


let getRandomInt = max =>
    Math.floor(Math.random() * Math.floor(max));


let MockEnum = arr => () => {
    let { lenght } = arr
    let index = getRandomInt(lenght - 1)
    return arr[index]
}


let id = () => _.random.number() 
let firstName = () => _.name.firstName()
let lastName = () => _.name.lastName()
let email = () => _.internet.email()
let phone = () => _.phone.phoneNumber()


let Doctor = () => ({
    id,
    firstName,
    lastName,
    email,
    phone,
    appointments : new MockList([5, 10]),
})


let Appointment = () => ({
    id,
    dateTime : Date.now,
    symptoms : new MockList([2, 5]),
})


let Duration = () => ({
    seconds : () => _.random.number(),
    zero : false,
    negative : false,
    nano : () => _.random.number(),
    units : new MockList([1, 3]),
})


let Patient = () => ({
    id,
    firstName,
    lastName,
    email,
    phone,
    healthRecords : new MockList([0, 10]),
    profileImage : _.image.people(),
    birthday : () => '01.01.1990',
})


let Location = () => ({
    latitude : () => _.address.latitude(),
    longitude : () => _.address.longitude(),
})


let Address = () => ({
    street : () => _.address.streetName(),
    city : () => _.address.city(),
    number : () => _.random.number(),
})


let Sickness = () => ({
    id,
    name : _.lorem.word(),
    description : _.lorem.paragraph(),
    symptoms : new MockList([1, 5]),
})


let BodyPart = 
    [ 'HEAD'
    , 'BACK'
    , 'LEFT_FEET'
    , 'RIGHT_FEET'
    , 'RIGHT_ARM'
    , 'LEFT_ARM'
    , 'TUMMY'
    , 'TORSO'
    , 'WHOLE'
    ]


let Symptom = () => ({
    id,
    name : () => _.lorem.words(),
    description : () => _.lorem.paragraph(),
    affectedBodyPart : MockEnum(BodyPart),
})


let TemporalUnit = () => {

    let bool = _.random.boolean()

    return ({
        dateBased : bool,
        timeBased : !bool,
        durationEstimated : _.random.boolean()
    })
}


let HealthRecord = () => ({
    id
})


let Query = () => ({
    appointments : () => new MockList([4, 10]),
    symptoms : () => new MockList([10, 20]),
})


let Mutation = () => ({
    scheduleAppointment (_, args) {
        console.log('Schedule Appointment', args)
        return 1
    }
})


module.exports = {
    Query,
    Mutation,
    Doctor,
    Patient,
    String : () => _.lorem.words(),
    Appointment,
    Duration,
    Location,
    Address,
    Sickness,
    Symptom,
    TemporalUnit,
    HealthRecord,
}