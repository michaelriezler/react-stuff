let {
  makeExecutableSchema,
  addMockFunctionsToSchema,
} = require('graphql-tools')


let mocks = require('./mocks')


let typeDefs = `
    type Query {
        doctor(id : Int!) : Doctor
        appointment(id : Int!) : Appointment
        
        appointments(
            id : Int!,
            role : UserRole!,
            filter : AppointmentType,
            date : Float,
        ) : [Appointment]
        
        healthRecords : [HealthRecord]
        sickness(id : Int!) : Sickness
        symptom(id : Int) : Symptom
        symptoms(bodyPart : BodyPart) : [Symptom]
        patient(id: Int!) : Patient
        nextAppointment(patientId : Int!) : Appointment
    }


    type Mutation {
        scheduleAppointment (
            lng         : Float,
            lat         : Float,
            symptoms    : [Int],
            patient     : Int,
            doctor      : Int,
            appointment : String,
            duration    : Int  
        ) : Int
    }


    type Doctor {
        id           : Int
        firstName    : String
        lastName     : String
        email        : String
        telephone    : String
        appointments : [Appointment]
    }


    type Appointment {
        id       : Int
        doctor   : Doctor
        patient  : Patient
        dateTime : String
        duration : Duration
        symptoms : [Symptom]
        canceled : Boolean
    }


    type Duration {
        seconds  : Int
        zero     : Boolean
        negative : Boolean
        units    : [TemporalUnit]
        nano     : Int
    }


    type HealthRecord {
        id                : Int
        actualSymptoms    : [Symptom]
        diagnosedSickness : Sickness
        appointment       : Appointment
    }


    type Patient {
        id            : Int
        firstName     : String
        lastName      : String
        email         : String
        phone         : String
        address       : Address
        healthRecords : [HealthRecord]
        location      : Location
        profileImage  : String
        birthday      : String
        insurance     : String
    }


    type Location {
        latitude  : Int
        longitude : Int   
    }


    type Address {
        street : String
        city   : String
        number : Int
        postal : Int
    }


    type Sickness {
        id          : Int
        name        : String
        description : String
        symptoms    : [Symptom]    
    }


    type Symptom {
        id          : Int
        name        : String
        description : String
        affectedBodyPart : BodyPart
    }


    type TemporalUnit {
        duration          : Duration
        dateBased         : Boolean
        durationEstimated : Boolean
        timeBased         : Boolean     
    }


    enum UserRole {
        PATIENT
        DOC
    }


    enum AppointmentType {
        FREE
        ALL
    }


    enum BodyPart {
        HEAD
        BACK
        LEFT_FEET
        RIGHT_FEET
        RIGHT_ARM
        LEFT_ARM
        TUMMY
        TORSO
        WHOLE
    }


`


let schema = makeExecutableSchema({ typeDefs })

addMockFunctionsToSchema({ schema, mocks })

module.exports = schema